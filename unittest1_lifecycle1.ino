/*
 Prueba unitaria control de ciclo de vida de la maquina.
 
 Autores:
 	David Correas Oliver
    Roberto Adrian Toaza Castro
*/

int buttonState = 0;

int lastButtonState = 0;

int buttonPushCounter = 0;

void setup()
{
  pinMode(2, INPUT);
  Serial.begin(9600);

  pinMode(13, OUTPUT);
  Serial.println("Machine state: Stop");
}

void loop()
{
  // Lectura del estado del boton
  buttonState = digitalRead(2);
  
  // Comprobamos si ha cambiado el estado
  if (buttonState != lastButtonState) {
    if (buttonState == HIGH) {
      // Estado de off a on
      buttonPushCounter += 1;
      Serial.println("Pulsado");
      Serial.print("Numero de pulsaciones: ");
      Serial.println(buttonPushCounter);
      
      if(buttonPushCounter % 2 == 0) {
        digitalWrite(13, LOW);
        Serial.println("Machine state: Stop");
      }
      else {
        digitalWrite(13, HIGH);
        Serial.println("Machine state: Start");
      }
    } else {
      // Boton soltado
      Serial.println("Soltado");
    }
    // Evitar debouncing
    delay(5); 
  }
  lastButtonState = buttonState;
}