/*
 Prueba unitaria control de prendas en la maquina.
 
 Autores:
 	David Correas Oliver
    Roberto Adrian Toaza Castro
*/
int sensorValue = 900;
int sensorState = 1;
unsigned long time;
unsigned long timeout;
int lastSensorState = 0;
int buttonPushCounter = 0;
int machineState = 0;

void setup()
{
  pinMode(A0, INPUT);
  Serial.begin(9600);

  pinMode(13, OUTPUT);
  Serial.println("Machine state: Stop");
}

void loop()
{
  // Lectura del estado del boton
  sensorValue = analogRead(A0);
  if (sensorValue < 750)
    sensorState = 0;
  else
    sensorState = 1;
  
  // Comprobamos si ha cambiado el estado
  if (sensorState != lastSensorState) {
    if (sensorState == 0) {
      // Estado de prenda colocada
      time = millis();
      buttonPushCounter += 1;
      Serial.println("Posado");
      Serial.print("Numero de pulsaciones: ");
      Serial.println(buttonPushCounter);
    } else {
      // Boton soltado
      Serial.println("Retirado");
      Serial.println("Machine state: Stop");
    }
    // Evitar debouncing
    delay(50); 
  }
  lastSensorState = sensorState;
  
  if (sensorState == 0) {
    timeout = millis() - time;
    if (timeout > 1500 && machineState == 0){
      Serial.println("Machine state: Start");
      machineState = 1;
      digitalWrite(13, HIGH);
    }
  }
  else{
    machineState = 0;
    digitalWrite(13, LOW);
  }
}