/*
 Prueba unitaria control de texto por pantalla.
 
 Autores:
 	David Correas Oliver
    Roberto Adrian Toaza Castro
*/

#include <LiquidCrystal.h>
#define INPUT_BUFFER_SIZE 50

// Definiciones
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
static char inputBuffer[INPUT_BUFFER_SIZE];

// Setup
void setup() {
  lcd.begin(16, 2);
  Serial.begin(9600);
}

// Controlamos la entrada por monitor serie
void handleInput() {
  if (Serial.available() > 0) {
    byte result = Serial.readBytesUntil('\n', inputBuffer, INPUT_BUFFER_SIZE);
    inputBuffer[result] = 0;
    pritnString(inputBuffer, result);
  }
}

// Funcion que imprime por pantalla haciendo scroll
void pritnString(char* inputBuffer, byte commandLength){
  lcd.setCursor(0, 0);
  int thisChar = 0;
  while(inputBuffer[thisChar] != 0){
    lcd.print(inputBuffer[thisChar]);
    delay(50);
    thisChar = thisChar + 1;
  }

  lcd.setCursor(16, 1);
  lcd.autoscroll();
  int thisChar2 = 0;
  while(inputBuffer[thisChar2] != 0){
    lcd.print(" ");
    delay(300);
    thisChar2 = thisChar2 + 1;
  }
  
  lcd.noAutoscroll();
  lcd.clear();
}

// Loop
void loop() {
  handleInput();
}
 