/*
 Autores:
  David Correas Oliver
  Roberto Adrian Toaza Castro
*/

#include <LiquidCrystal.h>
#include <Servo.h>
#define INPUT_BUFFER_SIZE 50

// ################################################
// ################# Declaracion ##################
// ################################################

LiquidCrystal lcd(12, 11, 8, 4, 7, 2);
static char inputBuffer[INPUT_BUFFER_SIZE];
int pos = 0;
char sensorChar[] = {'0', '1'};

// --------- Boton ciclo vida ---------
int lastButtonState = 0;

// --------- Sensores ciclo vida ---------
int sensorState0;
int sensorState1;
int lastSensorState0 = 1;
int lastSensorState1 = 1;
int machineState0 = 0;
int machineState1 = 0;
unsigned long timeout;
unsigned long time0;
unsigned long time1;

// --------- Paletas ---------
Servo servo_R;
Servo servo_L;
Servo servo_CU;
Servo servo_CD;
Servo servo_D;

// ################################################
// #################### Setup #####################
// ################################################
void setup() {
  // ------ Pantalla ------
  lcd.begin(16, 2);
  
  // ------ Sensor luz ------
  pinMode(A0, INPUT);
  
  // ------ Boton ciclo vida -----
  pinMode(13, INPUT);
  
  // ------ Serialport ------
  Serial.begin(9600);

  // ----------- Boards-------------
  //------------Board R------------ #0
  servo_R.attach(6);
  servo_R.write(0);
  //------------Bpard L------------ #1
  servo_L.attach(3);
  servo_L.write(0);
  //-------Board Center Up--------- #2
  servo_CU.attach(5);
  servo_CU.write(0);
  //-------Board Center Down------- #3
  servo_CD.attach(9);
  servo_CD.write(0);
  //---------Board Down------------ #4
  servo_D.attach(10);
  servo_D.write(0);
}

// ################################################
// ################### Pantalla ###################
// ################################################

// Funcion que imprime por pantalla haciendo scroll
void pritnString(char* inputBuffer, byte commandLength){
  lcd.setCursor(0, 0);
  int thisChar = 0;
  while(inputBuffer[thisChar] != 0){
    lcd.print(inputBuffer[thisChar]);
    delay(50);
    thisChar = thisChar + 1;
  }

  lcd.setCursor(16, 1);
  lcd.autoscroll();
  int thisChar2 = 0;
  while(inputBuffer[thisChar2] != 0){
    lcd.print(" ");
    delay(300);
    thisChar2 = thisChar2 + 1;
  }
  
  lcd.noAutoscroll();
  lcd.clear();
}

void printTwoLines(char* up, char* down, char sensor){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(up);
  lcd.setCursor(0, 1);
  lcd.print(sensor);
  lcd.print(down);
}

// ################################################
// ################ Sensor activacion #############
// ################################################

bool isActivated(int sensor){
  int sensorState;
  int machineState;
  int time;

  if (sensor == 0) {
    sensorState = sensorState0;
    machineState = machineState0;
    time = time0;
  }
  else {
    sensorState = sensorState1;
    machineState = machineState1;
    time = time1;
  }

  if (sensorState == 0) {
    int timeout;
    timeout = millis() - time;
    if (timeout > 1500 && machineState == 0){
      printTwoLines("ESPERANDO...", ": Desbloqueado", sensorChar[sensor]);
      Serial.print(sensor);
      Serial.println(": Machine state: Start");
      machineState = 1;
    }
  }
  else 
    machineState = 0;
  
  if (sensor == 0)
      machineState0 = machineState;
    else
      machineState1 = machineState;

  if (machineState == 1)
    return true;
  else
    return false; 
}

void setActivateTime(int sensorState, int lastSensorState, int sensor){
  unsigned long time = 0;

  if (sensorState != lastSensorState) {
    if (sensorState == 0) {
      // Estado de prenda colocada
      unsigned long time = millis();
      printTwoLines("ESPERANDO...", ": Posado", sensorChar[sensor]);
      Serial.print(sensor);
      Serial.println(": Posado");
      if (sensor == 0)
        time0 = time;
      else
        time1 = time;
    } else {
      // ensor destapado
      printTwoLines("ESPERANDO...", ": Retirado", sensorChar[sensor]);
      Serial.print(sensor);
      Serial.println(": Retirado");
    }
    // Evitar debouncing
    delay(20); 
  }
  if (sensor == 0)
    lastSensorState0 = sensorState;
  else
    lastSensorState1 = sensorState;
}

int getSensorState(int sensor){
  int sensorValue;
  int sensorState;

  if (sensor == 0)
    sensorValue = analogRead(A0);
  else
    sensorValue = analogRead(A1);
  
  if (sensorValue < 750)
    sensorState = 0;
  else
    sensorState = 1;

  return sensorState;
}

// ################################################
// ################## Control boton ###############
// ################################################

bool isPressed(){

  while (true) {
    // Lectura del estado del boton
    int buttonState = digitalRead(13);
    
    // Comprobamos si ha cambiado el estado
    if (buttonState != lastButtonState) {
        
      if (buttonState == HIGH) {
        // Estado de off a on
        printTwoLines("BOTON:", "> Pulsado", '-');
        Serial.println("Pulsado");
      } else {
        // Boton soltado
        printTwoLines("BOTON:", "> Soltado", '-');
        Serial.println("Soltado");
        lastButtonState = buttonState;
        return true;
      }
      // Evitar debouncing
      delay(5);
    }
    else {
      return false;
    }
    lastButtonState = buttonState;
  }
}

// ################################################
// ################# Secuencias ###################
// ################################################

bool moverPaleta(int n){
  Servo servo;
	switch(n){
		case 0:
    	servo = servo_R;
      break;
		case 1:
  		servo = servo_L;
      break;
		case 2:
  		servo = servo_CU;
      break;
		case 3:
  		servo = servo_CD;
      break;
		case 4:
  		servo = servo_D;
      break;
	}
  for (pos = 0; pos <= 180; pos += 1) {
    if(isPressed()){
      printTwoLines("PROCESO ABORTADO:", "> Boton pulsado", '-');
      Serial.println("PROCESO ABORTADO");
      servo.write(0);
      return false;
    }
    servo.write(pos);
    delay(15); 
  }
  for (pos = 180; pos >= 0; pos -= 1) {
    if(isPressed()){
      printTwoLines("PROCESO ABORTADO:", "> Boton pulsado", '-');
      Serial.println("PROCESO ABORTADO");
      servo.write(0);
      return false;
    }
    servo.write(pos);
    delay(15);
  }
  return true;
}

void secuencia_doblado(){
  if (!moverPaleta(4)) {
    waitForRestore();
    return;
  }
  if (!moverPaleta(0)) {
    waitForRestore();
    return;
  }
  if (!moverPaleta(1)) {
    waitForRestore();
    return;
  }
  if (!moverPaleta(0)) {
    waitForRestore();
    return;
  }
  if (!moverPaleta(3)) {
    waitForRestore();
    return;
  }
  if (!moverPaleta(2)) {
    waitForRestore();
    return;
  }
  delay(2500);
  printTwoLines("FINALIZADO", "Con exito*", '*');
}

void waitForRestore(){
  delay(2500);
  printTwoLines("Para continuar:", "pulse el boton", ' ');
  while(true){
    if(isPressed())
      return;
  }
}

void waitForClothing(){
  while (true) {  
    // ------ COMPROBAR BOTON ---------
    if(isPressed()){
      printTwoLines("START...", "> Boton pulsado", '-');
      Serial.println("Boton pulsado");
      resetSensors();
      return; 
    }
    
    // ------ COMPROBAR SENSORES ---------
    sensorState0 = getSensorState(0);
    sensorState1 = getSensorState(1);
  
    setActivateTime(sensorState0, lastSensorState0, 0);
    setActivateTime(sensorState1, lastSensorState1, 1);
  
    if(isActivated(0) && isActivated(1)){
      printTwoLines("START...", "> Sensores", '-');
      Serial.println("sensores activados");
      resetSensors();
      return;
    }
    if (isActivated(1))
      continue;
  }
}

void resetSensors(){
  lastSensorState0 = 1;
  lastSensorState1 = 1;
  machineState0 = 0;
  machineState1 = 0;
}

// ################################################
// #################### Loop ######################
// ################################################

void loop() {

  printTwoLines("ESPERANDO...", "Coloque prenda>", '<');
  Serial.println("Maquina en espera");
  // Fase espera
  waitForClothing();

  Serial.println("Maquina funcionando");

  secuencia_doblado();
  delay(2500);
}
 