/*
 Prueba unitaria control de movimiento de una paleta.
 
 Autores:
 	David Correas Oliver
    Roberto Adrian Toaza Castro
*/

#include <Servo.h>
#define INPUT_BUFFER_SIZE 50

static char inputBuffer[INPUT_BUFFER_SIZE];
int pos = 0;

Servo servo_9;

void setup()
{
  servo_9.attach(9);
  Serial.begin(9600);
  servo_9.write(0);
  Serial.println("Escriba '1' para iniciar");
}

void handleInput() {
  if (Serial.available() > 0) {
    byte result = Serial.readBytesUntil('\n', inputBuffer, INPUT_BUFFER_SIZE);
    inputBuffer[result] = 0;
    interpretCommand(inputBuffer, result);
  }
}

void interpretCommand(char* inputBuffer, byte commandLength) {
  if (inputBuffer[0] == '1') {
    Serial.println("Activar paleta");
    moverPaleta();
    Serial.println("Accion terminada");
  }
}

int moverPaleta(){
  Serial.println("Posicion 0 -> 180");
  for (pos = 0; pos <= 180; pos += 1) {
    servo_9.write(pos);
    delay(15); 
  }
  Serial.println("Posicion 180 -> 0");
  for (pos = 180; pos >= 0; pos -= 1) {
    servo_9.write(pos);
    delay(15);
  }
}

void loop()
{
  handleInput();
}
